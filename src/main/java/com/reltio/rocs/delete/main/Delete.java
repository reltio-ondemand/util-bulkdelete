package com.reltio.rocs.delete.main;

import com.google.gson.Gson;
import com.reltio.cst.exception.handler.APICallFailureException;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.util.Util;
import com.reltio.rocs.delete.dto.RelationsResponse;
import com.reltio.rocs.delete.dto.ScanResponseURI;
import com.reltio.rocs.delete.dto.Uri;
import com.reltio.rocs.delete.executor.*;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static com.reltio.cst.util.Util.waitForTasksReady;
import static com.reltio.rocs.util.LineNumberReaderHelper.readLines;

public class Delete {

    private static final Logger LOG = LoggerFactory.getLogger(Delete.class);

    private static String URIS_FILE_NAME = "uris.txt";

    private ReltioAPIService service;

    public ReltioAPIService getService() {
        return service;
    }

    public void setService(ReltioAPIService service) {
        this.service = service;
    }

    public static void main(String[] args) throws Exception {

        LOG.info("Process Started...");

        long start = System.currentTimeMillis();

        String propertyFilePath = "";
        Properties config = null;

        if (args.length == 0) {
            LOG.error("Configuration file path is not passed as argument");
            return;
        } else {
            propertyFilePath = args[0];

            if (args.length >= 2 && (!("y".equalsIgnoreCase(args[1]) || "yes".equalsIgnoreCase(args[1])))) {

                LOG.error("Delete flag is not set as Yes(Y) hence Quitting!!");
                System.exit(0);
            }

            if (!new File(propertyFilePath).exists()) {
                LOG.error("\n\nConfiguration File does not exist -- " + propertyFilePath);
                System.exit(0);
            }

            config = Util.getProperties(args[0], "PASSWORD", "CLIENT_CREDENTIALS");

            LOG.info("Processing with configuration file = " + propertyFilePath + "\n");

            if (LOG.isDebugEnabled()) {
                LOG.debug("Processing with configuration file = " + propertyFilePath);
            }

            LOG.info("Your configuration is as follows.");
            Set<String> keys = config.stringPropertyNames();

            for (String key : keys) {
                if (!"PASSWORD".equalsIgnoreCase(key) && !"CLIENT_CREDENTIALS".equalsIgnoreCase(key)) {
                    LOG.info(key + " : " + config.getProperty(key));
                }
            }

            if (args.length == 1) {

                Scanner sc = new Scanner(System.in);
                LOG.info("\n\nThis tool will delete records that are mentioned in file or by filter. You will have to"
                        + " restore from backup to get the records back.\nAre you sure? Yes(y)/No(n): ");

                String option = sc.nextLine();

                if (!("y".equalsIgnoreCase(option) || "yes".equalsIgnoreCase(option))) {
                    LOG.error("Quitting !!");
                    System.exit(0);
                }

                Util.close(sc);
            }
        }

        Util.setHttpProxy(config);

        Map<List<String>, List<String>> mutualExclusiveProps = new HashMap<>();

        mutualExclusiveProps.put(Arrays.asList(new String[]{"PASSWORD", "USERNAME"}), Arrays.asList(new String[]{"CLIENT_CREDENTIALS"}));

        List<String> missingKeys = Util.listMissingProperties(config,
                Arrays.asList("AUTH_URL", "ACTION", "ENVIRONMENT_URL", "INPUT", "TENANT_ID"), mutualExclusiveProps);

        String outputFile = config.getProperty("OUTPUT_FILE", "output.txt");

        if (!missingKeys.isEmpty()) {
            LOG.info("Following properties are missing from configuration file!! \n" + missingKeys);
            System.exit(0);
        }

        Files.deleteIfExists(Paths.get(outputFile));

        Delete util = initialize(config);

        ReltioAPIService restApi = util.getService();
        BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));

        String action = config.getProperty("ACTION");

        LOG.info("Action = " + action + " , input is  = " + config.getProperty("INPUT"));

        long size = 0;

        String steps = config.getProperty("STEPS");

        switch (action) {

            case "filter": {

                if (steps == null) {

                    size = util.scanEntitiesByFilter(config, writer, restApi);

                    config.put("INPUT", URIS_FILE_NAME);

                    size = util.deleteByUriList(config, writer, restApi);
                } else {

                    if (steps != null && steps.contains("SCAN")) {

                        size = util.scanEntitiesByFilter(config, writer, restApi);
                    }

                    if (steps != null && steps.contains("DELETE")) {

                        config.put("INPUT", URIS_FILE_NAME);
                        size = util.deleteByUriList(config, writer, restApi);
                    }
                }

                break;
            }

            case "uris":

                util.validateFileExistance(config);
                size = util.deleteByUriList(config, writer, restApi);

                break;

            case "crosswalksuris":

                util.validateFileExistance(config);
                size = util.deleleteCrossWalksByUris(config, writer, restApi);

                break;

            case "relationsByType":

            {
                if (steps == null) {

                    size = util.scanRelationsByFilter(config, writer, restApi);

                    config.put("INPUT", URIS_FILE_NAME);

                    size = util.deleteRelationsByUris(config, writer, restApi);
                } else {

                    if (steps != null && steps.contains("scan")) {

                        size = util.scanRelationsByFilter(config, writer, restApi);
                    }

                    if (steps != null && steps.contains("delete")) {

                        config.put("INPUT", URIS_FILE_NAME);
                        size = util.deleteRelationsByUris(config, writer, restApi);
                    }
                }
            }

            break;

            case "sourcesystem":

            {
                if (steps == null) {

                    size = util.scanSourceSystemCrossWalks(config, writer, restApi);

                    config.put("INPUT", URIS_FILE_NAME);

                    size = util.deleleteCrossWalksByUris(config, writer, restApi);

                } else {

                    if (steps != null && steps.contains("scan")) {

                        size = util.scanSourceSystemCrossWalks(config, writer, restApi);
                    }

                    if (steps != null && steps.contains("delete")) {

                        config.put("INPUT", URIS_FILE_NAME);
                        size = util.deleleteCrossWalksByUris(config, writer, restApi);
                    }
                }
            }

            break;

            case "relationbycrosswalks":

                util.validateFileExistance(config);
                size = util.deleteRelationsByCrosswalks(config, writer, restApi);

                break;

            case "relationsByEntitiesUris":

                util.validateFileExistance(config);
                size = util.deleteRelationsByEntitiesUris(config, writer, restApi);
                break;

            case "relationsuris":

                util.validateFileExistance(config);
                size = util.deleteRelationsByUris(config, writer, restApi);

                break;

            case "crosswalks": {

                util.validateFileExistance(config);
                size = util.deleteCrossWalksBySourceSystemValue(config, writer, restApi);

                break;
            }

            case "interactions": {

                util.validateFileExistance(config);
                size = util.deleteInteractionsByEnityUris(config, writer, restApi);

                break;
            }

            case "relationsByEntitiesCrossWalks": {

                util.validateFileExistance(config);
                size = util.deleteRelationsByEntitiesCrossWalks(config, writer, restApi);

                break;
            }

            default:

                LOG.error("\n\nAction mentioned is not in avaliable action = " + action
                        + "\n\nAvailable actions \n[filter, uris, crosswalks, sourcesystem, relationsByType, relationbycrosswalks, relationsByEntitiesCrossWalks, relationsuris]");
        }

        long end = System.currentTimeMillis();

        long seconds = TimeUnit.MILLISECONDS.toSeconds(end - start);

        LOG.info("Process completed in " + seconds + ", Records Processed = " + size + ", OPS = " + (size / seconds));

        Util.close(writer);
    }

    /**
     * This method deletes the relations by entities crosswalks
     * <p>
     * Input will be
     * <p>
     * RELATION_TYPE#sourcesystem#sourcesystemvalue#sourcetable#sourcesystem#sourcesystemvalue#sourcetable
     */
    protected long deleteRelationsByEntitiesCrossWalks(Properties config, BufferedWriter writer,
                                                       ReltioAPIService restApi) throws Exception {

        String input = config.getProperty("INPUT");

        int threads = NumberUtils.toInt(config.getProperty("THREAD_COUNT"), 100);
        int bulk = NumberUtils.toInt(config.getProperty("RECORDS_PER_POST"), 75);

        ExecutorService executorService = Executors.newFixedThreadPool(threads);

        List<Future<Long>> futures = new ArrayList<Future<Long>>();

        writer.write("Deleting from content of the file as uris = " + input + System.lineSeparator());

        writer.flush();

        FileReader reader = new FileReader(input);
        BufferedReader buffer = new BufferedReader(reader);

        List<String> sourceInfos = readLines(bulk, buffer);

        long size = sourceInfos.size();

        while (sourceInfos.size() != 0) {

            DeleteExecutor executor = new DeleteRelationsByEntitiesCrossWalksExecutor(writer, sourceInfos, restApi,
                    config);
            futures.add(executorService.submit(executor));

            waitForTasksReady(futures, threads * 2);

            LOG.info("Delete records = " + size);

            sourceInfos = readLines(bulk, buffer);
            size += sourceInfos.size();
        }

        LOG.info("Waiting for threads to complete.");

        waitForTasksReady(futures, threads * 0);

        executorService.shutdown();

        Util.close(buffer, reader);

        return size;
    }

    protected long deleteInteractionsByEnityUris(Properties config, BufferedWriter writer, ReltioAPIService restApi)
            throws Exception {

        String input = config.getProperty("INPUT");

        int threads = NumberUtils.toInt(config.getProperty("THREAD_COUNT"), 100);
        int bulk = NumberUtils.toInt(config.getProperty("RECORDS_PER_POST"), 75);

        ExecutorService executorService = Executors.newFixedThreadPool(threads);

        List<Future<Long>> futures = new ArrayList<Future<Long>>();

        writer.write("Deleting from content of the file as uris = " + input + System.lineSeparator());

        writer.flush();

        FileReader reader = new FileReader(input);
        BufferedReader buffer = new BufferedReader(reader);

        List<String> sourceInfos = readLines(bulk, buffer);

        long size = sourceInfos.size();

        while (sourceInfos.size() != 0) {

            DeleteExecutor executor = new DeleteInteractionsExecutor(writer, sourceInfos, restApi, config);
            futures.add(executorService.submit(executor));

            waitForTasksReady(futures, threads * 2);

            LOG.info("Delete records = " + size);

            sourceInfos = readLines(bulk, buffer);
            size += sourceInfos.size();
        }

        LOG.info("Waiting for threads to complete.");

        waitForTasksReady(futures, threads * 0);

        executorService.shutdown();

        Util.close(buffer, reader);

        return size;

    }

    /**
     * This will delete cross walk uri based on the crosswalk value and source
     * system mentioned in file as SOURCE_SYSTEM_TYPE|SOURCE_SYSTEM_VALUE in each
     * line
     */
    protected long deleteCrossWalksBySourceSystemValue(Properties config, BufferedWriter writer,
                                                       ReltioAPIService restApi) throws Exception {

        String apiUrl = getApiUl(config);
        String delimiter = config.getProperty("DELIMITER", "#");

        int threads = NumberUtils.toInt(config.getProperty("THREAD_COUNT"), 100);
        int bulk = NumberUtils.toInt(config.getProperty("RECORDS_PER_POST"), 75);

        ExecutorService executorService = Executors.newFixedThreadPool(threads);

        List<Future<Long>> futures = new ArrayList<Future<Long>>();

        writer.write("Deleting from content of the file = " + config.getProperty("INPUT"));

        writer.newLine();
        writer.newLine();
        writer.flush();

        BufferedReader buffer = Util.getBufferedReader(config.getProperty("INPUT"));

        List<String> sourceInfos = readLines(bulk, buffer);

        long size = sourceInfos.size();

        while (sourceInfos.size() != 0) {

            DeleteExecutor executor = new DeleteCrossWalksSourceAndValueExecutor(writer, sourceInfos, restApi, apiUrl,
                    delimiter);
            futures.add(executorService.submit(executor));

            waitForTasksReady(futures, threads * 2);

            LOG.info("Delete records = " + size);

            sourceInfos = readLines(bulk, buffer);
            size += sourceInfos.size();
        }

        LOG.info("Waiting for threads to complete.");

        waitForTasksReady(futures, 0);

        executorService.shutdown();

        Util.close(buffer);

        return size;

    }

    /**
     * This method will scan source system cross walks source system will be
     * mentioned as INPUT in properties file
     */
    protected int scanSourceSystemCrossWalks(Properties config, BufferedWriter writer, ReltioAPIService restApi)
            throws Exception {

        String apiUrl = getApiUl(config);
        int bulk = NumberUtils.toInt(config.getProperty("RECORDS_PER_POST"), 75);

        int threads = NumberUtils.toInt(config.getProperty("THREAD_COUNT"), 100);
        int count = 0;
        ExecutorService executorService = Executors.newFixedThreadPool(threads);

        List<Future<Long>> futures = new ArrayList<Future<Long>>();

        String input = config.getProperty("INPUT");

        String entityType = config.getProperty("ENTITY_TYPE");

        String totalUrl = apiUrl + "entities/_total?filter=equals(sourceSystems,'" + input + "')";

        String scanUrl = apiUrl + "entities/_scan?filter=equals(sourceSystems,'" + input + "')";

        if (!Util.isEmpty(entityType)) {
            totalUrl = totalUrl + " and equals(type," + entityType + ")";
            scanUrl = scanUrl + " and equals(type," + entityType + ")";
        }
        scanUrl = scanUrl + "&select=crosswalks&max=" + bulk;
        String totalscanUrlResponse = restApi.get(totalUrl);

        LOG.info(totalscanUrlResponse);

        BufferedWriter uriWriter = Util.getBufferedWriter(URIS_FILE_NAME);

        Gson gson = new Gson();

        boolean isDone = false;

        String intialJSON = "";

        while (!isDone) {

            for (int i = 0; i < threads; i++) {

                String scanResponse = restApi.post(scanUrl, intialJSON);

                if (!Util.isEmpty(scanResponse)) {

                    ScanResponseURI scanResponseObjUri = gson.fromJson(scanResponse, ScanResponseURI.class);

                    final List<Uri> uris = scanResponseObjUri.getObjects();

                    if (Util.isNotEmpty(uris)) {

                        DeleteExecutor executor = new WriteSourceSystemCrosswalksUris(uriWriter, uris, restApi, apiUrl,
                                input);

                        count += uris.size();
                        LOG.info("Scanned records = " + count);
                        futures.add(executorService.submit(executor));

                    } else {

                        isDone = true;
                        break;
                    }

                    scanResponseObjUri.setObjects(null);

                    intialJSON = gson.toJson(scanResponseObjUri.getCursor());

                    intialJSON = "{\"cursor\":" + intialJSON + "}";
                }
            }

            waitForTasksReady(futures, threads * 2);
        }

        waitForTasksReady(futures, 0);

        executorService.shutdown();

        return count;

    }

    /**
     * This method will scan all relations by filter
     */
    protected int scanRelationsByFilter(Properties config, BufferedWriter writer, ReltioAPIService restApi)
            throws Exception {

        String apiUrl = getApiUl(config);
        String input = config.getProperty("INPUT");
        int threads = NumberUtils.toInt(config.getProperty("THREAD_COUNT"), 100);
        int bulk = NumberUtils.toInt(config.getProperty("RECORDS_PER_POST"), 100);

        ExecutorService executorService = Executors.newFixedThreadPool(threads);
        BufferedWriter uriWriter = Util.getBufferedWriter(URIS_FILE_NAME);

        List<Future<Long>> futures = new ArrayList<Future<Long>>();

        writer.write("Scanning from input as relation type = " + input);

        writer.newLine();
        writer.newLine();

        final String scanUrl = apiUrl + "relations/_dbscan";

        writer.write("Scan url for entities = " + scanUrl + "\n\n");

        writer.flush();

        Gson gson = new Gson();

        boolean isDone = false;

        String intialJSON = "{\"type\":\"configuration/relationTypes/TYPE\",\"returnUriOnly\":true,\"pageSize\":" + bulk
                + ",  \"timeLimit\":100000}";

        intialJSON = intialJSON.replaceAll("TYPE", input);

        int count = 0;

        while (!isDone) {

            for (int i = 0; i < threads; i++) {

                String scanResponse = restApi.post(scanUrl, intialJSON);

                if (!Util.isEmpty(scanResponse)) {

                    RelationsResponse scanResponseObjUri = gson.fromJson(scanResponse, RelationsResponse.class);

                    final List<String> uris = scanResponseObjUri.getUris();

                    if (Util.isNotEmpty(uris)) {

                        count += uris.size();

                        DeleteExecutor executor = new WriteRelationsByUrisExecutor(writer, uris, restApi, apiUrl,
                                uriWriter);

                        LOG.info("Scanned relations = " + count);
                        futures.add(executorService.submit(executor));

                    } else {

                        isDone = true;
                        break;
                    }

                    scanResponseObjUri.setUris(null);

                    intialJSON = gson.toJson(scanResponseObjUri.getCursor());
                }
            }

            waitForTasksReady(futures, threads * 2);
        }

        waitForTasksReady(futures, 0);

        executorService.shutdown();
        LOG.info("No of relations scanned = " + count);

        return count;
    }

    /**
     * This will deletes relations by uri mentioned in file
     */
    protected long deleteRelationsByUris(Properties config, BufferedWriter writer, ReltioAPIService restApi)
            throws Exception {

        String apiUrl = getApiUl(config);
        String input = config.getProperty("INPUT");

        int threads = NumberUtils.toInt(config.getProperty("THREAD_COUNT"), 100);
        int bulk = NumberUtils.toInt(config.getProperty("RECORDS_PER_POST"), 75);

        ExecutorService executorService = Executors.newFixedThreadPool(threads);

        List<Future<Long>> futures = new ArrayList<Future<Long>>();

        writer.write("Deleting from content of the file as uris = " + input);

        writer.newLine();
        writer.newLine();
        writer.flush();

        FileReader reader = new FileReader(input);
        BufferedReader buffer = new BufferedReader(reader);

        List<String> sourceInfos = readLines(bulk, buffer);

        long size = sourceInfos.size();

        while (sourceInfos.size() != 0) {

            DeleteExecutor executor = new DeleteRelationsByUrisExecutor(writer, sourceInfos, restApi, apiUrl);
            futures.add(executorService.submit(executor));

            waitForTasksReady(futures, threads * 2);

            LOG.info("Delete records = " + size);

            sourceInfos = readLines(bulk, buffer);
            size += sourceInfos.size();
        }

        LOG.info("Waiting for threads to complete.");

        waitForTasksReady(futures, threads * 0);

        executorService.shutdown();

        Util.close(buffer, reader);

        return size;
    }

    /**
     * This method validates existence of file mentioned in configuration file
     */
    protected void validateFileExistance(Properties config) {

        String file = config.getProperty("INPUT");

        if (file == null) {
            LOG.error("file name is required as inout field in properties file");
            System.exit(0);
        }
        if (!new File(file).exists()) {
            LOG.error("file does not exist with name = " + file + " , Please pass proper file name to process request");
            System.exit(0);
        }
    }

    /**
     * This will scan the records either based on source and entity type or by
     * filter specified in property configuration.
     *
     * @param config
     * @param writer
     * @param restApi
     */
    protected int scanEntitiesByFilter(Properties config, BufferedWriter writer, ReltioAPIService restApi)
            throws Exception {

        int threads = NumberUtils.toInt(config.getProperty("THREAD_COUNT"), 100);

        ExecutorService executorService = Executors.newFixedThreadPool(threads);

        List<Future<Long>> futures = new ArrayList<Future<Long>>();

        BufferedWriter uriWriter = Util.getBufferedWriter(URIS_FILE_NAME);
        int count = 0;
        Gson gson = new Gson();

        boolean isDone = false;

        String intialJSON = "";

        String apiUrl = getApiUl(config);

        final String totalUrl = getSearchUrl(config, "total");

        String totalscanUrlResponse = restApi.get(totalUrl);

        LOG.info(totalscanUrlResponse);

        final String scanUrl = getSearchUrl(config, "scan");

        writer.write("Scan url for entities = " + scanUrl);
        writer.newLine();
        writer.newLine();

        writer.flush();

        LOG.info("Scan url for entities = " + scanUrl);

        while (!isDone) {

            for (int i = 0; i < threads; i++) {

                String scanResponse = restApi.post(scanUrl, intialJSON);

                if (!Util.isEmpty(scanResponse)) {

                    ScanResponseURI scanResponseObjUri = gson.fromJson(scanResponse, ScanResponseURI.class);

                    final List<Uri> uris = scanResponseObjUri.getObjects();

                    if (Util.isNotEmpty(uris)) {

                        WriteUriExecutor executor = new WriteUriExecutor(uriWriter, uris, restApi, apiUrl);

                        count += uris.size();
                        futures.add(executorService.submit(executor));

                    } else {

                        isDone = true;
                        break;
                    }

                    scanResponseObjUri.setObjects(null);

                    intialJSON = gson.toJson(scanResponseObjUri.getCursor());

                    intialJSON = "{\"cursor\":" + intialJSON + "}";
                }

                LOG.info("Scanned Records count = " + count);
                writer.write("Scanned Records count = " + count + "\n");
                writer.newLine();
            }

            waitForTasksReady(futures, threads * 2);
        }

        waitForTasksReady(futures, 0);

        executorService.shutdown();

        Util.close(uriWriter);

        return count;

    }

    /**
     * This will delete cross walks uri mentioned in the file in each file
     *
     * @param config
     * @param writer
     * @param restApi
     */

    protected long deleleteCrossWalksByUris(Properties config, BufferedWriter writer, ReltioAPIService restApi)
            throws Exception {

        String apiUrl = getApiUl(config);
        String inputFile = config.getProperty("INPUT");

        int bulk = NumberUtils.toInt(config.getProperty("RECORDS_PER_POST"), 75);
        int threads = NumberUtils.toInt(config.getProperty("THREAD_COUNT"), 100);

        ExecutorService executorService = Executors.newFixedThreadPool(threads);

        List<Future<Long>> future = new ArrayList<Future<Long>>();

        writer.write("Deleted from content of the file as uris = " + inputFile);

        writer.newLine();
        writer.newLine();

        writer.flush();

        FileReader reader = new FileReader(inputFile);

        LineNumberReader buffer = new LineNumberReader(reader);

        List<String> sourceInfos = readLines(bulk, buffer);

        long size = sourceInfos.size();

        while (sourceInfos.size() != 0) {

            for (int i = 0; i < threads; i++) {

                DeleteExecutor executor = new DeleteCrossWalksUriExecutor(writer, restApi, apiUrl, sourceInfos);

                future.add(executorService.submit(executor));

                LOG.info("Delete records = " + size);

                sourceInfos = readLines(bulk, buffer);
                size += sourceInfos.size();

                if (sourceInfos.size() == 0) {
                    break;
                }
            }

            waitForTasksReady(future, threads * 2);

        }

        waitForTasksReady(future, 0);

        executorService.shutdown();

        Util.close(buffer);

        return size;
    }

    /**
     * This will delete relations by their cross walks mentioned in the file
     *
     * @param config
     * @param writer
     * @param service
     */

    protected long deleteRelationsByCrosswalks(Properties config, BufferedWriter writer, ReltioAPIService service)
            throws Exception {

        String apiUrl = getApiUl(config);

        int threads = NumberUtils.toInt(config.getProperty("THREAD_COUNT"), 100);
        int bulk = NumberUtils.toInt(config.getProperty("RECORDS_PER_POST"), 75);

        ExecutorService executorService = Executors.newFixedThreadPool(threads);

        List<Future<Long>> future = new ArrayList<Future<Long>>();

        writer.write("Deleted from content of the file as uris = " + config.getProperty("INPUT"));

        writer.newLine();
        writer.newLine();

        writer.flush();

        FileReader reader = new FileReader(config.getProperty("INPUT"));

        LineNumberReader buffer = new LineNumberReader(reader);

        List<String> sourceInfos = readLines(bulk, buffer);

        long size = sourceInfos.size();

        while (sourceInfos.size() != 0) {

            DeleteExecutor executor = new DeleleteRelationsByCrossWalkExecutor(writer, service, apiUrl, sourceInfos);

            future.add(executorService.submit(executor));

            waitForTasksReady(future, 2);
            LOG.info("Delete records = " + size);

            sourceInfos = readLines(bulk, buffer);
            size = +sourceInfos.size();
        }

        LOG.info("Waiting for threads to complete.");
        waitForTasksReady(future, 0);

        executorService.shutdown();

        Util.close(reader, buffer);
        return size;

    }

    /**
     * This will delete entities by uris mentioned in the file
     *
     * @param config
     * @param writer
     * @param restApi
     */
    protected long deleteByUriList(Properties config, BufferedWriter writer, ReltioAPIService restApi)
            throws Exception {

        LOG.debug("deleteByUriList started");

        String apiUrl = getApiUl(config);
        String input = config.getProperty("INPUT");

        int threads = NumberUtils.toInt(config.getProperty("THREAD_COUNT"), 100);
        int bulk = NumberUtils.toInt(config.getProperty("RECORDS_PER_POST"), 150);

        ExecutorService executorService = Executors.newFixedThreadPool(threads);

        List<Future<Long>> futures = new ArrayList<Future<Long>>();

        writer.write("Deleted from content of the file as uris = " + input);

        writer.newLine();
        writer.newLine();
        writer.flush();

        FileReader reader = new FileReader(input);
        BufferedReader buffer = new BufferedReader(reader);

        List<String> uris = readLines(bulk, buffer);

        int size = uris.size();

        while (uris.size() != 0) {

            DeleteExecutor executor = new DeleteEntityExecutor(writer, restApi, apiUrl, uris);
            futures.add(executorService.submit(executor));

            uris = readLines(bulk, buffer);
            size += uris.size();
            LOG.info("Delete records = " + size);
            waitForTasksReady(futures, threads * 2);
        }

        LOG.info("Waiting for threads to complete.");
        waitForTasksReady(futures, 0);

        executorService.shutdown();

        Util.close(buffer, reader);

        LOG.debug("deleteByUriList ended");

        return size;
    }

    /**
     * This will delete relation between entities and the entities cross walks of
     * the 2 entities involved in the relations mentioned in the file
     *
     * @param config
     * @param writer
     * @param restApi
     */
    protected long deleteRelationsByEntitiesUris(Properties config, BufferedWriter writer, ReltioAPIService restApi)
            throws Exception {
        LOG.debug("deleteRelationsByEntitiesUris starts");

        String apiUrl = getApiUl(config);
        String input = config.getProperty("INPUT");

        int threads = NumberUtils.toInt(config.getProperty("THREAD_COUNT"), 100);
        int bulk = NumberUtils.toInt(config.getProperty("RECORDS_PER_POST"), 75);

        ExecutorService executorService = Executors.newFixedThreadPool(threads);

        List<Future<Long>> futures = new ArrayList<Future<Long>>();

        writer.write("Deleted from content of the file as uris = " + input);

        writer.newLine();
        writer.newLine();
        writer.flush();

        FileReader reader = new FileReader(input);
        BufferedReader buffer = new BufferedReader(reader);

        List<String> sourceInfos = readLines(bulk, buffer);

        long size = sourceInfos.size();

        while (sourceInfos.size() != 0) {

            DeleteExecutor executor = new DeleteRelationsByEntitiesUrisExecutor(writer, restApi, apiUrl, sourceInfos);
            futures.add(executorService.submit(executor));

            waitForTasksReady(futures, threads * 2);
            LOG.info("Delete records = " + size);
            sourceInfos = readLines(bulk, buffer);
            size += sourceInfos.size();
        }

        LOG.info("Waiting for threads to complete.");
        waitForTasksReady(futures, 0);

        executorService.shutdown();

        Util.close(buffer, reader);
        LOG.debug("deleteRelationsByEntitiesUris ends");
        return size;
    }

    /**
     * This will validate credentials
     */
    private static Delete initialize(Properties properties) {

        Delete util = new Delete();

        try {

            Map<List<String>, List<String>> mutualExclusiveProps = new HashMap<>();

            mutualExclusiveProps.put(Arrays.asList(new String[]{"PASSWORD", "USERNAME"}), Arrays.asList(new String[]{"CLIENT_CREDENTIALS"}));
            util.setService(Util.getReltioService(properties));
        } catch (APICallFailureException e) {
            LOG.error("Error Code: " + e.getErrorCode() + " >>>> Error Message: " + e.getErrorResponse());
            System.exit(0);
        } catch (GenericException e) {
            LOG.error(e.getExceptionMessage());
            System.exit(0);
        } catch (Exception e) {
            LOG.error("Failed to get reltio service", e);
            System.exit(0);
        }

        return util;
    }

    /**
     * This method will returns either total URL or scan URL based on the properties
     * mentioned in configuration file
     */
    protected String getSearchUrl(Properties config, String type) {

        String url = "";

        String apiUrl = getApiUl(config);
        String input = config.getProperty("INPUT");

        int bulk = NumberUtils.toInt(config.getProperty("RECORDS_PER_POST"), 75);

        if ("scan".equals(type)) {

            if (input != null) {
                url = apiUrl + "entities/_scan?filter=" + input + "&select=uri&max=" + bulk;
            }

        } else if ("total".equals(type)) {

            if (input != null) {
                url = apiUrl + "entities/_total?filter=" + input;
            }
        }

        return url;
    }

    /**
     * This will prepare api url from ENVIRONMENT_URL and TENANT_ID
     */
    protected String getApiUl(Properties config) {
        return "https://" + config.getProperty("ENVIRONMENT_URL") + "/reltio/api/" + config.getProperty("TENANT_ID")
                + "/";
    }

    /**
     * This will print performance of the operations
     */
    public void printPorformance(long start, long records) {

        long end = System.currentTimeMillis();

        long totalTime = TimeUnit.MILLISECONDS.toSeconds(end - start);

        LOG.info("OPS = " + (records / totalTime));
    }
}
