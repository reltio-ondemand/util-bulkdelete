
package com.reltio.rocs.delete.dto;

import java.util.List;


public class Interactions {

    
    private List<Interaction> interactions = null;
   
    private Integer totalFetched;
    
    private Boolean fetchedAll;

     
    public List<Interaction> getInteractions() {
        return interactions;
    }

    
    public void setInteractions(List<Interaction> interactions) {
        this.interactions = interactions;
    }

    public Integer getTotalFetched() {
        return totalFetched;
    }

    public void setTotalFetched(Integer totalFetched) {
        this.totalFetched = totalFetched;
    }

    public Boolean getFetchedAll() {
        return fetchedAll;
    }

    public void setFetchedAll(Boolean fetchedAll) {
        this.fetchedAll = fetchedAll;
    }

}
