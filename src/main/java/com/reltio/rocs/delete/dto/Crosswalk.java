package com.reltio.rocs.delete.dto;

import java.util.List;

/**
 * 
 * @author Ganesh.Palanisamy@reltio.com Created : Sep 19, 2014
 */
public class Crosswalk {

	private String uri;
	
	private String type;
	
	private String sourceTable;
	
	private String value;
	
	private String createDate;
	
	private String updateDate;
	
	private String deleteDate;
	
	private List<String> attributes;
	
	private String reltioLoadDate;
	/**
	 * @return the uri
	 */
	public String getUri() {
		return uri;
	}
	/**
	 * @param uri the uri to set
	 */
	public void setUri(String uri) {
		this.uri = uri;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	/**
	 * @return the createDate
	 */
	public String getCreateDate() {
		return createDate;
	}
	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	/**
	 * @return the updateDate
	 */
	public String getUpdateDate() {
		return updateDate;
	}
	/**
	 * @param updateDate the updateDate to set
	 */
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	/**
	 * @return the deleteDate
	 */
	public String getDeleteDate() {
		return deleteDate;
	}
	/**
	 * @param deleteDate the deleteDate to set
	 */
	public void setDeleteDate(String deleteDate) {
		this.deleteDate = deleteDate;
	}
	/**
	 * @return the attributes
	 */
	public List<String> getAttributes() {
		return attributes;
	}
	/**
	 * @param attributes the attributes to set
	 */
	public void setAttributes(List<String> attributes) {
		this.attributes = attributes;
	}
	/**
	 * @return the reltioLoadDate
	 */
	public String getReltioLoadDate() {
		return reltioLoadDate;
	}
	/**
	 * @param reltioLoadDate the reltioLoadDate to set
	 */
	public void setReltioLoadDate(String reltioLoadDate) {
		this.reltioLoadDate = reltioLoadDate;
	}

	public String getsourceTable() {
		return  sourceTable;
	}
	
	public void setSourceTable(String sourceTable) {
		this.sourceTable = sourceTable;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Crosswalk{");
		sb.append("uri='").append(uri).append('\'');
		sb.append(", type='").append(type).append('\'');
		sb.append(", sourceTable='").append(sourceTable).append('\'');
		sb.append(", value='").append(value).append('\'');
		sb.append(", createDate='").append(createDate).append('\'');
		sb.append(", updateDate='").append(updateDate).append('\'');
		sb.append(", deleteDate='").append(deleteDate).append('\'');
		sb.append(", attributes=").append(attributes);
		sb.append(", reltioLoadDate='").append(reltioLoadDate).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
