
package com.reltio.rocs.delete.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RelationResponse {

    @SerializedName("index")
    @Expose
    private int index;
    @SerializedName("object")
    @Expose
    private Object object;
    @SerializedName("successful")
    @Expose
    private boolean successful;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

}
