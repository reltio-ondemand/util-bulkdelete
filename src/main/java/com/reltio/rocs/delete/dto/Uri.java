package com.reltio.rocs.delete.dto;

import java.util.List;

public class Uri {

	private String uri;

	private List<Crosswalk> crosswalks;

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public List<Crosswalk> getCrosswalks() {
		return crosswalks;
	}

	public void setCrosswalks(List<Crosswalk> crosswalks) {
		this.crosswalks = crosswalks;
	}

	@Override
	public String toString() {
		return "Uri [uri=" + uri + ", crosswalks=" + crosswalks + "]";
	}
}