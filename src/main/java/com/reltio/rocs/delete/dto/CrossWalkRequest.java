/**
 * 
 */
package com.reltio.rocs.delete.dto;

/**
 * @author spatil
 *
 */
public class CrossWalkRequest {

	public CrossWalkRequest() {

	}

	public CrossWalkRequest(String type, String value, String sourceTable) {
		this.type = type;
		this.value = value;
		this.sourceTable = sourceTable;
	}

	protected String type;

	protected String value;

	protected String sourceTable;

	public String getSourceTable() {
		return sourceTable;
	}

	public void setSourceTable(String sourceTable) {
		this.sourceTable = sourceTable;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "CrossWalkRequest [type=" + type + ", value=" + value + ", sourceTable=" + sourceTable + "]";
	}

}
