package com.reltio.rocs.delete.dto;

import java.util.List;

public class ReltioEntities {

	protected List<ReltioObject> entities;

	public List<ReltioObject> getEntities() {
		return entities;
	}

	public void setEntities(List<ReltioObject> entities) {
		this.entities = entities;
	}

	@Override
	public String toString() {
		return "ReltioEntities [entities=" + entities + "]";
	}

}
