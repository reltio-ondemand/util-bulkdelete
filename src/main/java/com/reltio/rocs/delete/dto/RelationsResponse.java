package com.reltio.rocs.delete.dto;

import java.util.Collections;
import java.util.List;

public class RelationsResponse {

	 
	private Cursor cursor;
	
 	private List<String> uris = Collections.emptyList();
 
	public Cursor getCursor() {
		return cursor;
	}

	public void setCursor(Cursor cursor) {
		this.cursor = cursor;
	}

	public List<String> getUris() {
		return uris;
	}

	public void setUris(List<String> uris) {
		this.uris = uris;
	}

}
