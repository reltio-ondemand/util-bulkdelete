
package com.reltio.rocs.delete.dto;

 
public class Interaction {

    private String uri;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

	@Override
	public String toString() {
		return "Interaction [uri=" + uri + "]";
	}

}
