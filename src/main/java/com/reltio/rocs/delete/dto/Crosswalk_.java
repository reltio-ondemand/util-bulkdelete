
package com.reltio.rocs.delete.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Crosswalk_ {

	@SerializedName("value")
	@Expose
	private String value;

	@SerializedName("type")
	@Expose
	private String type;

	@SerializedName("sourceTable")
	@Expose
	private String sourceTable;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSourceTable() {
		return sourceTable;
	}

	public void setSourceTable(String sourceTable) {
		this.sourceTable = sourceTable;
	}

	@Override
	public String toString() {
		return "Crosswalk_ [value=" + value + ", type=" + type + ", sourceTable=" + sourceTable + "]";
	}

}
