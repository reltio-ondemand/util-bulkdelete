
package com.reltio.rocs.delete.dto;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RelationRequest {

    @SerializedName("objectUris")
    @Expose
    private List<String> objectUris = null;

    @SerializedName("relationTypes")
    @Expose
    private List<String> relationTypes = null;

    public List<String> getObjectUris() {
        return objectUris;
    }

    public void setObjectUris(List<String> objectUris) {
        this.objectUris = objectUris;
    }

    public List<String> getRelationTypes() {
        return relationTypes;
    }

    public void setRelationTypes(List<String> relationTypes) {
        this.relationTypes = relationTypes;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RelationRequest{");
        sb.append("objectUris=").append(objectUris);
        sb.append(", relationTypes=").append(relationTypes);
        sb.append('}');
        return sb.toString();
    }
}
