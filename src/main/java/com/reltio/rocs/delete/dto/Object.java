
package com.reltio.rocs.delete.dto;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Object {

	private String uri;

	@SerializedName("crosswalks")
	@Expose
	private List<Crosswalk> crosswalks = null;

	public List<Crosswalk> getCrosswalks() {
		return crosswalks;
	}

	public void setCrosswalks(List<Crosswalk> crosswalks) {
		this.crosswalks = crosswalks;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	@Override
	public String toString() {
		return "Object [uri=" + uri + ", crosswalks=" + crosswalks + "]";
	}

}
