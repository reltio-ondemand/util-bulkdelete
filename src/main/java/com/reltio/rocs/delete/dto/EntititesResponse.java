
package com.reltio.rocs.delete.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EntititesResponse {

	@SerializedName("index")
	@Expose
	private Integer index;

	@SerializedName("object")
	@Expose
	private Object object;

	@SerializedName("successful")
	@Expose
	private Boolean successful;

	@SerializedName("crosswalk")
	@Expose
	private Crosswalk_ crosswalk;

	@Expose
	private Errors errors;

	public Errors getErrors() {
		return errors;
	}

	public void setErrors(Errors errors) {
		this.errors = errors;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public Boolean getSuccessful() {
		return successful;
	}

	public void setSuccessful(Boolean successful) {
		this.successful = successful;
	}

	public Crosswalk_ getCrosswalk() {
		return crosswalk;
	}

	public void setCrosswalk(Crosswalk_ crosswalk) {
		this.crosswalk = crosswalk;
	}

	@Override
	public String toString() {
		return "EntititesResponse [index=" + index + ", object=" + object + ", successful=" + successful
				+ ", crosswalk=" + crosswalk + ", errors=" + errors + "]";
	}

}
