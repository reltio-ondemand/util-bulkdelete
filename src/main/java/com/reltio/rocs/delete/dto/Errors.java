package com.reltio.rocs.delete.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Errors {
	@SerializedName("severity")
	@Expose
	private String severity;
	@SerializedName("errorMessage")
	@Expose
	private String errorMessage;
	@SerializedName("errorCode")
	@Expose
	private Integer errorCode;
	@SerializedName("errorDetailMessage")
	@Expose
	private String errorDetailMessage;

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorDetailMessage() {
		return errorDetailMessage;
	}

	public void setErrorDetailMessage(String errorDetailMessage) {
		this.errorDetailMessage = errorDetailMessage;
	}
}
