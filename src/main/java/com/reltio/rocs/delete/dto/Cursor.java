package com.reltio.rocs.delete.dto;

public class Cursor {

	private String cursor;

	public String getCursor() {
		return cursor;
	}

	public void setCursor(String cursor) {
		this.cursor = cursor;
	}

}
