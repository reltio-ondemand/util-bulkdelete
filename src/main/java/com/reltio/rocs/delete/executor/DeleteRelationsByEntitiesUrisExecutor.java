/**
 * 
 */
package com.reltio.rocs.delete.executor;

import java.io.BufferedWriter;
import java.util.Collections;
import java.util.List;

import com.google.gson.Gson;
import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.rocs.delete.dto.Entites;

/**
 * @author spatil
 *
 */
public class DeleteRelationsByEntitiesUrisExecutor extends DeleteExecutor {

	private List<String> sourceInfos = Collections.emptyList();

	public DeleteRelationsByEntitiesUrisExecutor(BufferedWriter writer, ReltioAPIService restApi, String apiUrl,
			List<String> sourceInfos) {

		this.writer = writer;
		this.restApi = restApi;
		this.apiUrl = apiUrl;
		this.sourceInfos = sourceInfos;

	}

	@Override
	public Long call() throws Exception {

		String template = "{\"objectUris\":[\"START_OBJECT\",\"END_OBJECT\"],\"relationTypes\":[\"configuration/relationTypes/RELATION_TYPE\"]}";

		String url = apiUrl + "relations/getByObjectUris?returnUriOnly=true";

		for (String data : sourceInfos) {

			String request = template;

			String[] info = data.split("\\|");

			if (info.length != 3) {
				writer.write(
						"data is not proper format excepted as entities/ZtGW076|entities/VlZNOim|relationtype data provided as"
								+ data + DeleteExecutor.NEWLINE);

				writer.flush();

				continue;
			}

			request = request.replaceFirst("START_OBJECT", info[0]);
			request = request.replaceFirst("END_OBJECT", info[1]);
			request = request.replaceFirst("RELATION_TYPE", info[2]);

			try {

				String relationRespose = restApi.post(url, request);

				Entites[] response = new Gson().fromJson(relationRespose, Entites[].class);

				if (response.length == 0) {
					writer.write(data + " -- No such Relationship between entities" + DeleteExecutor.NEWLINE);

					writer.flush();
					continue;
				}

				Entites en = response[0];

				String deletResponse = restApi.delete(apiUrl + en.getEntitiy().getUri(), "");

				writer.write(data + " -- " + deletResponse + DeleteExecutor.NEWLINE);

			} catch (ReltioAPICallFailureException e) {
				writer.write(data + " -- " + e.getErrorResponse() + DeleteExecutor.NEWLINE);
			} catch (Exception e) {
				writer.write(data + " -- " + e.getMessage() + DeleteExecutor.NEWLINE);
			}

			writer.flush();
		}

		return System.currentTimeMillis();
	}

}
