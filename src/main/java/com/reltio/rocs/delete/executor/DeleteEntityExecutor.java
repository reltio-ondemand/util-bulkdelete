/**
 * * @author spatil
 * This callable executor which delete entities
 */
package com.reltio.rocs.delete.executor;

import java.io.BufferedWriter;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.rocs.delete.dto.Uri;
import com.reltio.rocs.util.Util;

/**
 * This class is Callable instance with help of the ReltioAPIService it deletes
 * entities passed in uris and uriList
 */
public class DeleteEntityExecutor extends DeleteExecutor {

	private List<Uri> uris = Collections.emptyList();

	private List<String> uriList = Collections.emptyList();

	public DeleteEntityExecutor(BufferedWriter writer, List<Uri> uris, ReltioAPIService restApi, String apiUrl) {

		this.writer = writer;
		this.uris = uris;
		this.restApi = restApi;
		this.apiUrl = apiUrl;
	}

	public DeleteEntityExecutor(BufferedWriter writer, ReltioAPIService restApi, String apiUrl, List<String> uriList) {

		this.writer = writer;
		this.uriList = uriList;
		this.restApi = restApi;
		this.apiUrl = apiUrl;
	}

	public List<Uri> getUris() {
		return uris;
	}

	public void setUris(List<Uri> uris) {
		this.uris = uris;
	}

	public List<String> getUriList() {
		return uriList;
	}

	public void setUriList(List<String> uriList) {
		this.uriList = uriList;
	}

	@Override
	/**
	 * @param writer
	 * @param count
	 * @param uris
	 * @param restApi
	 * @param apiUrl
	 * 
	 * @return {@link Callable} returns that can be submitted to executor
	 */
	public Long call() throws Exception {

		String url = apiUrl + "entities/_deleteByUris";

		Gson gson = new Gson();

		if (Util.isNotEmpty(uriList)) {

			try {

				String resp = restApi.post(url, gson.toJson(uriList));
				writer.write(url + "--" + resp + DeleteExecutor.NEWLINE);

			} catch (ReltioAPICallFailureException e) {
				writer.write(url + "--" + e.getErrorResponse() + DeleteExecutor.NEWLINE);
			} catch (Exception e) {
				writer.write(url + " -- " + e.getMessage() + DeleteExecutor.NEWLINE);
			}
		}

		if (Util.isNotEmpty(uris)) {

			try {

				List<String> entityUris = uris.stream().map(Uri::getUri).collect(Collectors.toList());

				String resp = restApi.post(url, gson.toJson(entityUris));

				writer.write(url + " -- " + resp + DeleteExecutor.NEWLINE);

			} catch (ReltioAPICallFailureException e) {
				writer.write(url + "--" + e.getErrorResponse() + DeleteExecutor.NEWLINE);
			} catch (Exception e) {
				writer.write(url + " -- " + e.getMessage() + DeleteExecutor.NEWLINE);
			}
		}

		writer.flush();

		return System.currentTimeMillis();
	}
}
