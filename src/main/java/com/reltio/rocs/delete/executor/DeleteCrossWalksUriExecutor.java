/**
 * 
 */
package com.reltio.rocs.delete.executor;

import java.io.BufferedWriter;
import java.util.List;

import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.cst.service.ReltioAPIService;

/**
 * 
 * @author spatil
 */
public class DeleteCrossWalksUriExecutor extends DeleteExecutor {

	protected List<String> sourceInfo;

	public DeleteCrossWalksUriExecutor(BufferedWriter writer, ReltioAPIService restApi, String apiUrl,
			List<String> sourceInfo) {

		this.apiUrl = apiUrl;
		this.writer = writer;
		this.sourceInfo = sourceInfo;
		this.restApi = restApi;
	}

	@Override
	public Long call() throws Exception {

		for (String uri : sourceInfo) {

			try {

				String deleteRessponse = restApi.delete(apiUrl + uri, "");
				writer.write(uri + "--" + deleteRessponse + DeleteExecutor.NEWLINE);

			} catch (ReltioAPICallFailureException e) {
				writer.write(uri + " -- " + e.getErrorResponse() + DeleteExecutor.NEWLINE);
			} catch (Exception e) {
				writer.write(uri + " -- " + e.getMessage() + DeleteExecutor.NEWLINE);
			} finally {
				writer.flush();
			}

		}

		return System.currentTimeMillis();
	}

}
