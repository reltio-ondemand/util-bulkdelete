package com.reltio.rocs.delete.executor;

import java.io.BufferedWriter;
import java.util.Collections;
import java.util.List;

import com.reltio.cst.service.ReltioAPIService;
/**
 *
 * @author spatil
 */
public class WriteRelationsByUrisExecutor extends DeleteExecutor {

	private List<String> sourceInfos = Collections.emptyList();

	private BufferedWriter uriWriter;

	public WriteRelationsByUrisExecutor(BufferedWriter writer, List<String> sourceInfos, ReltioAPIService restApi,
			String apiUrl, BufferedWriter uriWriter) {
		this.writer = writer;
		this.setSourceInfos(sourceInfos);
		this.restApi = restApi;
		this.apiUrl = apiUrl;
		this.uriWriter = uriWriter;
	}

	public List<String> getSourceInfos() {
		return sourceInfos;
	}

	public void setSourceInfos(List<String> sourceInfos) {
		this.sourceInfos = sourceInfos;
	}

	@Override
	public Long call() throws Exception {
		uriWriter.write(String.join(DeleteExecutor.NEWLINE, sourceInfos) + DeleteExecutor.NEWLINE);
		uriWriter.flush();
		return System.currentTimeMillis();
	}
}