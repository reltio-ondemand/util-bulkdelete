package com.reltio.rocs.delete.executor;

import java.io.BufferedWriter;
import java.util.Collections;
import java.util.List;

import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.rocs.delete.dto.Crosswalk;
import com.reltio.rocs.delete.dto.Uri;

/**
 * This Callable class deletes cross walk in an entity
 **/
public class DeleteCrossWalkExecutor extends DeleteExecutor {

	private List<Uri> uris = Collections.emptyList();

	private String sourceSystem;

	public DeleteCrossWalkExecutor(BufferedWriter writer, List<Uri> uris, ReltioAPIService restApi, String apiUrl,
			String sourceSystem) {

		this.writer = writer;
		this.uris = uris;
		this.restApi = restApi;
		this.apiUrl = apiUrl;
		this.sourceSystem = sourceSystem;
	}

	@Override
	public Long call() throws Exception {

		String sourceSystem = "configuration/sources/" + this.sourceSystem;

		for (Uri uri : uris) {

			try {

				List<Crosswalk> crossWalks = uri.getCrosswalks();

				for (Crosswalk crossWalk : crossWalks) {

					if (sourceSystem.equals(crossWalk.getType())) {
						String deleteResponse = deleteCrosswalk(crossWalk, uri.getUri());
						writer.write(uri.getUri() + " -- " + deleteResponse + DeleteExecutor.NEWLINE);
					}
				}

			} catch (ReltioAPICallFailureException e) {
				writer.write(uri.getUri() + "--" + e.getErrorResponse() + DeleteExecutor.NEWLINE);
			} catch (Exception e) {
				writer.write(uri.getUri() + " -- " + e.getMessage() + DeleteExecutor.NEWLINE);
			}

			writer.flush();
		}

		return System.currentTimeMillis();
	}

	protected String deleteCrosswalk(Crosswalk crosswalk, String uri) throws Exception {

		String url = apiUrl + crosswalk.getUri();
		String resp = restApi.delete(url, "");
		return resp;
	}
}