package com.reltio.rocs.delete.executor;

import java.io.BufferedWriter;
import java.util.List;

import com.reltio.cst.service.ReltioAPIService;
import com.reltio.rocs.delete.dto.Crosswalk;
import com.reltio.rocs.delete.dto.Uri;
/**
 *
 * @author spatil
 */
public class WriteSourceSystemCrosswalksUris extends DeleteExecutor {

	private List<Uri> uris;

	private String sourceSystem;

	public WriteSourceSystemCrosswalksUris(BufferedWriter writer, List<Uri> uris, ReltioAPIService restApi,
			String apiUrl, String sourceSystem) {
		this.uris = uris;
		this.sourceSystem = sourceSystem;
		this.apiUrl = apiUrl;
		this.writer = writer;
	}

	@Override
	public Long call() throws Exception {

		String sourceSystem = "configuration/sources/" + this.sourceSystem;

		for (Uri uri : uris) {

			try {

				List<Crosswalk> crossWalks = uri.getCrosswalks();

				for (Crosswalk crossWalk : crossWalks) {

					if (sourceSystem.equals(crossWalk.getType())) {

						writer.write(crossWalk.getUri() + DeleteExecutor.NEWLINE);
					}
				}

			} catch (Exception e) {
				System.out.println(e);
			} finally {
				writer.flush();
			}
		}

		return System.currentTimeMillis();
	}
}