package com.reltio.rocs.delete.executor;

import java.io.BufferedWriter;
import java.util.Properties;
import java.util.concurrent.Callable;

import com.reltio.cst.service.ReltioAPIService;

/**
 *
 * @author spatil
 */
public abstract class DeleteExecutor implements Callable<Long> {

	protected BufferedWriter writer;

	protected ReltioAPIService restApi;

	protected String apiUrl;

	public BufferedWriter getWriter() {
		return writer;
	}

	public void setWriter(BufferedWriter writer) {
		this.writer = writer;
	}

	public ReltioAPIService getRestApi() {
		return restApi;
	}

	public void setRestApi(ReltioAPIService restApi) {
		this.restApi = restApi;
	}

	public String getApiUrl() {
		return apiUrl;
	}

	public void setApiUrl(String apiUrl) {
		this.apiUrl = apiUrl;
	}

	public static final String NEWLINE = System.lineSeparator();

	protected String getEntityId(String uri) {

		if (uri.startsWith("\"") && uri.endsWith("\"")) {
			uri = uri.substring(1, uri.length() - 1);
		}

		return uri.startsWith("entities") ? uri : "entities/" + uri;
	}

	protected String getApiURL(Properties config) {
		return "https://" + config.getProperty("ENVIRONMENT_URL") + "/reltio/api/" + config.getProperty("TENANT_ID")
				+ "/";
	}
}