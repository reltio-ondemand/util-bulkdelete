package com.reltio.rocs.delete.executor;

import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.rocs.delete.dto.CrossWalkRequest;
import com.reltio.rocs.delete.dto.Crosswalk;
import com.reltio.rocs.delete.dto.EntititesResponse;
/**
 *
 * @author spatil
 */
public class DeleteCrossWalksSourceAndValueExecutor extends DeleteExecutor {

	protected List<String> sourceInfos;

	protected String delimiter;

	public DeleteCrossWalksSourceAndValueExecutor(BufferedWriter writer, List<String> sourceInfos,
			ReltioAPIService restApi, String apiUrl, String delimiter) {

		this.restApi = restApi;
		this.apiUrl = apiUrl;
		this.sourceInfos = sourceInfos;
		this.writer = writer;
		this.delimiter = delimiter;
	}

	@Override
	public Long call() throws Exception {

		List<CrossWalkRequest> request = prepareCrosswalkJsonRequest();

		String result = new Gson().toJson(request);

		String enitityResponse = restApi.post(apiUrl + "/entities/_byCrosswalks?select=crosswalks,uri", result);

		EntititesResponse[] response = new Gson().fromJson(enitityResponse, EntititesResponse[].class);

		for (int i = 0; i < response.length; i++) {

			EntititesResponse entity = response[i];

			if (!entity.getSuccessful()) {

				String error = entity.getErrors() != null ? entity.getErrors().getErrorMessage()
						: "Failed to Get Crosswalk Info";
				writer.write(entity.getCrosswalk().getType() + delimiter + entity.getCrosswalk().getValue() + delimiter
						+ entity.getCrosswalk().getSourceTable() + " -- " + error + DeleteExecutor.NEWLINE);
				writer.flush();

				continue;
			}

			com.reltio.rocs.delete.dto.Object object = entity.getObject();

			List<Crosswalk> crosswalks = object.getCrosswalks();
			CrossWalkRequest crossWalkRequest = request.get(i);

			for (Crosswalk walk : crosswalks) {

				if (crossWalkRequest.getType().equals(walk.getType())
						&& crossWalkRequest.getValue().equals(walk.getValue())) {

					try {

						String apiResponse = restApi.delete(apiUrl + walk.getUri(), "");
						writer.write(walk.getUri() + " -- " + apiResponse + DeleteExecutor.NEWLINE);
						break;
					} catch (ReltioAPICallFailureException e) {
						writer.write(walk.getUri() + " -- " + e.getErrorResponse() + DeleteExecutor.NEWLINE);
					} catch (Exception e) {
						writer.write(walk.getUri() + " -- " + e.getMessage() + DeleteExecutor.NEWLINE);
					} finally {
						writer.flush();
					}
				}
			}
		}

		return System.currentTimeMillis();
	}

	/**
	 * This will prepare CrossWalkRequest to get entity response in bulk
	 * 
	 */
	protected List<CrossWalkRequest> prepareCrosswalkJsonRequest() {

		List<CrossWalkRequest> request = new ArrayList<>();

		for (String key : sourceInfos) {

			String[] split = key.split(delimiter);
			CrossWalkRequest entry = new CrossWalkRequest("configuration/sources/" + split[0], split[1],
					split.length == 3 ? split[2] : null);

			request.add(entry);
		}

		return request;
	}
}