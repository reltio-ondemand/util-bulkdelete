/**
 * 
 */
package com.reltio.rocs.delete.executor;

import java.io.BufferedWriter;
import java.util.List;
import java.util.stream.Collectors;

import com.reltio.cst.service.ReltioAPIService;
import com.reltio.rocs.delete.dto.Uri;

/**
 * @author spatil
 *
 */
public class WriteUriExecutor extends DeleteEntityExecutor {

	public WriteUriExecutor(BufferedWriter writer, List<Uri> uris, ReltioAPIService restApi, String apiUrl) {
		super(writer, uris, null, null);
	}

	public WriteUriExecutor(BufferedWriter writer, ReltioAPIService restApi, String apiUrl, List<String> uriList) {
		super(writer, null, apiUrl, uriList);
	}

	@Override
	/**
	 * @param writer
	 * @param count
	 * @param uris
	 * @param restApi
	 * @param apiUrl
	 * 
	 * @return {@link Callable} returns that can be submitted to executor
	 */
	public Long call() throws Exception {

		List<String> entityUris = getUris().stream().map(Uri::getUri).collect(Collectors.toList());

		writer.write(String.join("\n", entityUris)+ DeleteExecutor.NEWLINE);
 		writer.flush();

		return System.currentTimeMillis();
	}
}
