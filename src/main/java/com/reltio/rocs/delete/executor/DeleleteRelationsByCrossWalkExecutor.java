package com.reltio.rocs.delete.executor;

import java.io.BufferedWriter;
import java.util.List;

import com.google.gson.Gson;
import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.rocs.delete.dto.CrossWalkRequest;
import com.reltio.rocs.delete.dto.Crosswalk;
import com.reltio.rocs.delete.dto.Relation;
/**
 *
 * @author spatil
 */
public class DeleleteRelationsByCrossWalkExecutor extends DeleteExecutor {

	protected List<String> sourceInfos;
	


	public DeleleteRelationsByCrossWalkExecutor(BufferedWriter writer, ReltioAPIService service, String apiUrl,
			List<String> sourceInfos) {

		this.writer = writer;
		this.restApi = service;
		this.apiUrl = apiUrl;
		this.sourceInfos = sourceInfos;
		System.out.println("delete");

	}

	@Override
	public Long call() throws Exception {

		String url = apiUrl + "relations/_byCrosswalk/CROSSWALK_ID?type=SOURCESYSTEM&sourceTable=ST&select=uri";

		Gson gson = new Gson();

		for (String data : sourceInfos) {


			String[] info = data.split("\\#", -1); 
						
				String[] split = data.split("\\#");
				String sourceTable = (split.length ==3) ? "&sourceTable=" +split[2] : "";				
				String relUrl = apiUrl + "relations/_byCrosswalk/" + split[1] + "?type=" + split[0] + sourceTable +"&select=uri";

			try {

				String response = restApi.get(relUrl);

				Relation ent = gson.fromJson(response, Relation.class);

				for (Crosswalk entry : ent.getCrosswalks()) {

					if (info[1].equals(entry.getValue())) {
						String deleteResponse = restApi.delete(apiUrl + entry.getUri(), "");

						writer.write(data + " -- " + deleteResponse + DeleteExecutor.NEWLINE);
					}
				}

			} catch (ReltioAPICallFailureException e) {
				writer.write(data + " -- " + e.getErrorResponse() + DeleteExecutor.NEWLINE);
			} catch (Exception e) {
				writer.write(data + " -- " + e.getMessage() + DeleteExecutor.NEWLINE);
			}

			writer.flush();
		}

		return System.currentTimeMillis();
	}
}