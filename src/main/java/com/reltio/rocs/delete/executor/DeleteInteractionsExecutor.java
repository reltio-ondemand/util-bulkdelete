package com.reltio.rocs.delete.executor;

import java.io.BufferedWriter;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.rocs.delete.dto.Interaction;
import com.reltio.rocs.delete.dto.Interactions;
/**
 *
 * @author spatil
 */
public class DeleteInteractionsExecutor extends DeleteExecutor {

    private static final Logger LOG = LoggerFactory.getLogger(DeleteInteractionsExecutor.class);

    private List<String> uriList = Collections.emptyList();

    private Properties config;

    public DeleteInteractionsExecutor(BufferedWriter writer, List<String> sourceInfos, ReltioAPIService restApi,
                                      Properties config) {

        this.config = config;
        this.uriList = sourceInfos;
        this.writer = writer;
        this.restApi = restApi;
        this.apiUrl = "https://" + config.getProperty("ENVIRONMENT_URL") + "/reltio/api/"
                + config.getProperty("TENANT_ID") + "/";

    }

    @Override
    public Long call() throws Exception {

        for (String uri : uriList) {
            deleteEntitiesInteractions(getEntityId(uri));
        }

        return System.currentTimeMillis();
    }

    protected void deleteEntitiesInteractions(String uri) throws Exception {

        String interactionsUrl = getApiUrl() + uri + "/_interactions?select=uri";

        int max = NumberUtils.toInt(config.getProperty("RECORDS_PER_POST"), 50);

        String response = getRestApi().get(interactionsUrl + "&offset=0&max=" + max);
        int offset = max;

        Interactions interaction = new Gson().fromJson(response, Interactions.class);

        writer.write(uri + " do not have interactions, skipping" + NEWLINE);

        while (interaction.getInteractions() != null && interaction.getInteractions().size() != 0) {

            List<Interaction> interactions = interaction.getInteractions();

            for (Interaction itr : interactions) {

                try {

                    String deleteResponse = getRestApi().delete(getApiUrl() + itr.getUri(), null);
                    writer.write(getApiUrl() + itr.getUri() + "" + deleteResponse + NEWLINE);

                } catch (GenericException e) {
                    LOG.error("failed to delete interactions", e);
                    writer.write(getApiUrl() + itr.getUri() + "" + e.getExceptionMessage() + NEWLINE);
                } catch (Exception e) {
                    LOG.error("failed to delete interactions", e);
                    writer.write(getApiUrl() + itr.getUri() + "" + e.getMessage() + NEWLINE);
                } finally {
                    writer.flush();
                }
            }

            response = getRestApi().get(interactionsUrl + "&offset=" + offset + "&max=" + max);

            interaction = new Gson().fromJson(response, Interactions.class);
        }
    }

}