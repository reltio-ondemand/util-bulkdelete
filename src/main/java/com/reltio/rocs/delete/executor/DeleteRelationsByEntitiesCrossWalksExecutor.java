package com.reltio.rocs.delete.executor;

import com.google.gson.Gson;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.rocs.delete.dto.CrossWalkRequest;
import com.reltio.rocs.delete.dto.Entites;
import com.reltio.rocs.delete.dto.RelationRequest;
import com.reltio.rocs.delete.dto.RelationResponse;
import com.reltio.rocs.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.util.*;

/**
 * @author spatil
 */
public class DeleteRelationsByEntitiesCrossWalksExecutor extends DeleteExecutor {

    private static final Logger LOG = LoggerFactory.getLogger(DeleteRelationsByEntitiesCrossWalksExecutor.class);

    private static final String SOURCE_PREFIX = "configuration/sources/";
    private static final String RELATIONS_SHIP_PREFIX = "configuration/relationTypes/";


    protected List<String> sourceInfos;

    protected String delimiter;

    public DeleteRelationsByEntitiesCrossWalksExecutor(BufferedWriter writer, List<String> sourceInfos,
                                                       ReltioAPIService restApi, Properties config) {

        this.writer = writer;
        this.restApi = restApi;
        this.apiUrl = getApiURL(config);
        this.sourceInfos = sourceInfos;
        this.delimiter = config.getProperty("DELIMITER", "#");

    }

    @Override
    public Long call() throws Exception {

        Map<String, String> entitiesCrosswalksMap = getEntitiesCrosswalksMap();


         for (String key : sourceInfos) {

            String[] split = key.split(delimiter);

            if (split.length < 6) {
                writer.write("Value is not appropriate as it is not having exact no of the fields" + key + ""
                        + ", Please check quick start guide for this input");
                continue;
            }

            RelationRequest request = new RelationRequest();

            request.setRelationTypes(Arrays.asList(split[0].startsWith(RELATIONS_SHIP_PREFIX) ? split[0] : RELATIONS_SHIP_PREFIX + split[0]));

            String startObjectUri = entitiesCrosswalksMap.get(split[1] + split[2] + split[3]);
            String endObjectUri = entitiesCrosswalksMap.get(split[4] + split[5] + (split.length > 6 ? split[6] : ""));

            request.setObjectUris(Arrays.asList(startObjectUri, endObjectUri));

            String relation = restApi.post(apiUrl + "/relations/getByObjectUris", new Gson().toJson(request));

            RelationResponse[] relationResponses = new Gson().fromJson(relation, RelationResponse[].class);

            for (RelationResponse relationResponse : relationResponses) {
                String uri = relationResponse.getObject().getUri();
                String deleteResponse = restApi.delete(apiUrl + "/" + uri, "");
                LOG.debug("Delete response = " + apiUrl + "/" + uri + ", response = " + deleteResponse);
            }
        }

        writer.flush();

        return System.currentTimeMillis();
    }

    protected Map<String, String> getEntitiesCrosswalksMap() throws Exception {

        Map<String, String> entitiesCrosswalksMap = new HashMap<>();

        List<CrossWalkRequest> requests = prepareCrosswalkJsonRequest();

        String requestJson = new Gson().toJson(requests);

        String enitityResponse = restApi.post(apiUrl + "/entities/_byCrosswalks?select=crosswalks,uri", requestJson);

        Entites[] response = new Gson().fromJson(enitityResponse, Entites[].class);


        for (int i = 0; i < requests.size(); i++) {

            Entites entity = response[i];

            if (entity.getErrors() == null) {

                CrossWalkRequest request = requests.get(i);
                entitiesCrosswalksMap.put(request.getType() + request.getValue() + (Util.isEmpty(request.getSourceTable()) ? "" : request.getSourceTable()),
                        entity.getEntitiy().getUri());
            } else {
                //TODO write errors to different file
            }
        }

        return entitiesCrosswalksMap;
    }

    /**
     * This will prepare CrossWalkRequest to get entity response in bulk
     */
    protected List<CrossWalkRequest> prepareCrosswalkJsonRequest() throws Exception {

        List<CrossWalkRequest> request = new ArrayList<>();

        try {

            for (String key : sourceInfos) {

                String[] split = key.split(delimiter);

                if (split.length < 6) {
                    writer.write("Value is not appropriate as it is not having exact no of the fields" + key + ""
                            + ", Please check quick start guide for this input");
                    continue;
                }

                request.add(
                        new CrossWalkRequest(split[1].startsWith(SOURCE_PREFIX) ? split[1] : SOURCE_PREFIX + split[1], split[2], Util.isEmpty(split[3]) ? null : split[3]));

                request.add(
                        new CrossWalkRequest(split[4].startsWith(SOURCE_PREFIX) ? split[4] : SOURCE_PREFIX + split[4], split[5], split.length == 6 ? null : split[6]));

            }

        } catch (Exception e) {
            LOG.error("Not able to generate crosswalk request " + sourceInfos, e);
        }

        return request;
    }
}