/**
 * 
 */
package com.reltio.rocs.delete.executor;

import java.io.BufferedWriter;
import java.util.Collections;
import java.util.List;

import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.cst.service.ReltioAPIService;

/**
 * @author spatil
 *
 */
public class DeleteRelationsByUrisExecutor extends DeleteExecutor {

	private List<String> uris = Collections.emptyList();

	public DeleteRelationsByUrisExecutor(BufferedWriter writer, List<String> uris, ReltioAPIService restApi,
			String apiUrl) {

		this.writer = writer;
		this.uris = uris;
		this.restApi = restApi;
		this.apiUrl = apiUrl;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.concurrent.Callable#call()
	 */
	@Override
	public Long call() throws Exception {

		for (String uri : uris) {

			try {

				String resp = restApi.delete(apiUrl + uri, "");
				writer.write(uri + "--" + resp + DeleteExecutor.NEWLINE);

			} catch (ReltioAPICallFailureException e) {
				writer.write(uri + " -- " + e.getErrorResponse() + DeleteExecutor.NEWLINE);
			} catch (Exception e) {
				writer.write(uri + " -- " + e.getMessage() + DeleteExecutor.NEWLINE);
			} finally {
				writer.flush();
			}
		}

		return System.currentTimeMillis();
	}
}
