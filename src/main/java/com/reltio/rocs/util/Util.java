package com.reltio.rocs.util;

import java.io.Closeable;
import java.io.IOException;
import java.util.Collection;

public abstract class Util {

	public static boolean isEmpty(Collection<?> collection) {

		if (collection == null || collection.isEmpty()) {
			return true;
		}

		return false;
	}

	public static boolean isNotEmpty(Collection<?> collection) {

		if (collection == null || collection.isEmpty()) {
			return false;
		}
		return true;
	}

	public static boolean isEmpty(String data) {

		if (data == null || data.trim().length() == 0) {
			return true;
		}

		return false;
	}

	public static void close(Closeable... resources) {

		if (resources != null) {

			for (Closeable resource : resources)

				try {
					resource.close();
				} catch (IOException e) {
					System.out.println("failed to close resource " + e.getMessage());
				}
		}

	}
}