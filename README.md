
# BULK OPERATION FOR entities/relations

## Description
This utility Delete entities/relations based in file input sourcetype, relations ship type ,relations uri, entities uri, filter,interactions of the entities mentioned


##Change Log


```
#!plaintext

Last Update Date: 23/09/2019

'relationsByEntitiesCrossWalks' action introduced and existing actions are corrected

CLIENT_CREDENTIALS implementation has been introduced
```
```
#!plaintext

Last Update Date: 27/06/2019
Version: 1.9  group name standardised, delete old jars as it will be available in git versions
Introduced new action to delete interactions input is entity uris

```

```
#!plaintext

Last Update Date: 21/03/2019
Version: 1.0.8  Password encryption changes
Descr
```
#!plaintext

Last Update Date: 04/01/2019
Version: 1.0.7
Description: Fixed for https://reltio.jira.com/browse/CUST-3055 and added description for crosswalks action
```

```
#!plaintext

Last Update Date: 01/09/2018
Version: 1.0.0
Description: Initial version
```
##Contributing 
Please visit our [Contributor Covenant Code of Conduct](https://bitbucket.org/reltio-ondemand/common/src/a8e997d2547bf4df9f69bf3e7f2fcefe28d7e551/CodeOfConduct.md?at=master&fileviewer=file-view-default) to learn more about our contribution guidlines

## Licensing
```
#!plaintext
Copyright (c) 2017 Reltio

 

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

 

    http://www.apache.org/licenses/LICENSE-2.0  

 

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

See the License for the specific language governing permissions and limitations under the License.
```

## Quick Start 
To learn about dependencies, building and executing the tool view our [quick start](https://bitbucket.org/reltio-ondemand/util-bulkdelete/src/e273fdb5e5b886bcdb79a8e540ef1c8a78cb4c21/QuickStart.md?at=master&fileviewer=file-view-default).


