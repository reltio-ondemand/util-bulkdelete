# Quick Start 

##Building
The main method of the application is at the following   
path: com.reltio.rocs.delete.main.Delete

##Dependencies 

1) slf4j-api 1.7.25  
2) reltio-cst-core 1.4.7

##Parameters File Example

```
ACTION=sourcesystem
available_actions =[filter, uris, crosswalks, sourcesystem, relationsByType, relationbycrosswalks,relationsByEntitiesCrossWalks,relationsuris,interactions]
ENVIRONMENT_URL=sndbx.reltio.com
TENANT_ID=GwhNNYU1bNDD9XW
USERNAME=***
PASSWORD=***
AUTH_URL=https://auth.reltio.com/oauth/token
OUTPUT_FILE=output.txt
INPUT=DPT
RECORDS_PER_POST=50
THREAD_COUNT=30
DELIMITER=#
#This is considered when ACTION=sourcesystem
#Here it will delete source system crosswalks for only Location entity type
ENTITY_TYPE=Location
#Host of the proxy
HTTP_PROXY_HOST=
#Port for the proxy
HTTP_PROXY_PORT=

#This property used for to get the access token using client_credentials grant type. The value for this property can be obtained by encoding the client name and secret separated by colon in Base64 format. (clientname:clientsecret)
CLIENT_CREDENTIALS =

```


#How input varies based in ACTION specified 
```

1) When ACTION=filter
INPUT=equals(type,'configuration/entityTypes/HCP') and equals(sourceSystems,'CRM')

2) When ACTION=uris
INPUT=inputfiles/uriList.txt
Urilist.txt file should contains entityIDs line by line, like
entities/Yrewe3a
entities/erREe3z
entities/Sd3we32

3) When ACTION=crosswalks
INPUT=inputfiles/crosswalkFile.txt
DELIMITER=#

crosswalkFile.txt should contains line below

i) With Source table
HMS#324353#source_table_hcp

Here HMS is source system, 324353 is source system value and source_table_hcp is source table this is optional if source system table is not exist in crosswalk section

ii) Without Source table 
  
HMS#324353

4)When ACTION=sourcesystem
INPUT=IMS

5) When ACTION=relationsByType
INPUT=HasAddress

6) When ACTION=relationsuris
INPUT=relationsuris.txt
Content of relationsuris.txt looks like

relations/erereer

7) When ACTION=relationbycrosswalks

INPUT=relationbycrosswalks.txt
Content relationbycrosswalks.txt looks like

i) With Source table (if we want to delete Relation crosswalk by using the sourcetable)

SOURCESYSTEM#CROSSWALKVALUE#SOURCETABLE

ii) Without Source table 
  
SOURCESYSTEM#CROSSWALKVALUE

8) When ACTION=relationsByEntitiesCrossWalks
INPUT=relationsByEntitiesCrossWalks.txt
Content of the relationsByEntitiesCrossWalks.txt looks like
entities/ZtGW076|entities/VlZNOim|relationtype

8) When ACTION=crosswalksuris
INPUT=crosswalksuris.txt
Content of the crosswalksuris.txt looks like
entities/fto7A8q/crosswalks/1DIxdxMX6


9) When ACTION=interactions
INPUT=entities.csv
Content of the file would be entity uris it can be in 3 formats
entities/dhfdf4
"entities/dhfdf4"
"dhfdf4"
This action will delete all interactions that these enitities have

10)When ACTION=relationsByEntitiesCrossWalks
INPUT =entitiescrosswalks.txt
Content of the file would be entity uris it can be in 3 formats
RELATION_TYPE#startobject_sourcesystem#startobject_sourcesystemvalue#startobject_sourcetable#endobject_sourcesystem#endobject_sourcesystemvalue#endobject_sourcetable
Note: If the crosswalk do not have source table you need to pass empty like below
RELATION_TYPE#startobject_sourcesystem#startobject_sourcesystemvalue##endobject_sourcesystem#endobject_sourcesystemvalue#
```
##Executing

Command to start the utility in interactive mode
```
#!plaintext
Note:Please note that use latest jar from jar folder in bitbucket

#Windows

java -jar util-bulkdelete-{{version}}-jar-with-dependencies.jar “configuration.properties” y > $logfilepath$'

#Unix
java -jar util-bulkdelete-{{version}}-jar-with-dependencies.jar “configuration.properties” y> $logfilepath$'

```